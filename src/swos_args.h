/* Copyright (C) 2019, 2023 |Méso|Star> (contact@@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SWOS_ARGS_H
#define SWOS_ARGS_H

#include <rsys/math.h>
#include <rsys/rsys.h>
#include <limits.h> /* UINT_MAX */

enum swos_algorithm {
  SWOS_ALGO_WOS, /* Walk on sphere */
  SWOS_ALGO_RWALK /* Random walk */
};

enum super_shape_type {
  SHAPE_SPHEROID,
  SHAPE_DISCOID
};

struct swos_args {
  enum swos_algorithm algo;
  enum super_shape_type shape_type;
  unsigned long nslices; /* #subdivisions of the super shape around the Z axis */
  unsigned long nrealisations; /* #Monte-Carlo realisations */
  unsigned nthreads; /* #threads */
  float delta; /* Random walk numerical parameter */
  float epsilon; /* WoS numerical parameter */
  float shape_radius; /* Radius of the super shape */
  int dump_mesh; /* Define if the mesh geometry is dumped to stdout */
  int quit; /* Define if the application must be stopped */
  float theta, phi, ratio; /* Define the probe position */
};

static const struct swos_args SWOS_ARGS_DEFAULT = {
  SWOS_ALGO_WOS,  /* Algo */
  SHAPE_SPHEROID, /* Super shape used */
  256,            /* #slices */
  10000,          /* #realisations */
  UINT_MAX,       /* #threads */
  0,              /* Delta */
  -1,             /* Epsilon */
  1.f,            /* Radius */
  0,              /* Dump mesh */
  0,              /* Quit */
  0,0,0           /* Probe position */
};

extern LOCAL_SYM const char*
algorithm_to_string
  (const enum swos_algorithm algo);

extern LOCAL_SYM const char*
shape_type_to_string
  (const enum super_shape_type type);

extern LOCAL_SYM res_T
swos_args_init
  (struct swos_args* args,
   int argc,
   char** argv);

extern LOCAL_SYM void
swos_args_release
  (struct swos_args* args);

#endif /* SWOS_ARGS_H */
