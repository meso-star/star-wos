/* Copyright (C) 2019, 2023 |Méso|Star> (contact@@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SWOS_H
#define SWOS_H

#include "swos_args.h"
#include <rsys/rsys.h>

/* Forward declarations */
struct s3dut_mesh;
struct s3d_device;
struct s3d_scene;
struct mem_allocator;

/* Star-WoS data structure */
struct swos {
  struct s3dut_mesh* mesh;
  struct s3d_device* s3d;
  struct s3d_scene* scn;
  struct mem_allocator* allocator;

  size_t nrealisations;
  enum swos_algorithm algo;
  enum super_shape_type shape_type;
  float probe_pos[3];
  float theta, phi, ratio;
  float delta;
  float delta_rwalk;
  float max_shape_radius;
  float epsilon;
  unsigned nthreads;
  int dump_mesh;
};
#define SWOS_NULL__ {\
  NULL, NULL, NULL, NULL, 0, SWOS_ALGO_RWALK, SHAPE_SPHEROID,\
  {0,0,0}, 0, 0, 0, 0, 0, 0, 0, 0, 0\
}
static const struct swos SWOS_NULL = SWOS_NULL__;

extern LOCAL_SYM res_T
swos_init
  (struct mem_allocator* allocator, /* NULL <=> default allocator */
   struct swos_args* args,
   struct swos* swos);

extern LOCAL_SYM void
swos_release
  (struct swos* swos);

extern LOCAL_SYM res_T
swos_run
  (struct swos* swos);

#endif /* SWOS_H */
