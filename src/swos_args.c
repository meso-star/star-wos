/* Copyright (C) 2019, 2023 |Méso|Star> (contact@@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "swos_args.h"

#include <rsys/cstr.h>

#include <getopt.h>
#include <string.h>

#ifdef COMPILER_CL
#define strtok_r strtok_s
#endif

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_help(const char* cmd)
{
  #define B(Str) "\x1b[1m"STR(Str)"\x1b[0m" /* Bold */
  #define U(Str) "\x1b[4m"STR(Str)"\x1b[0m" /* Underline */

  ASSERT(cmd);
  printf(
"Usage: "B(%s)" ["U(OPTION)" ...]\n"
"Monte Carlo diffusion in a super shape\n",
    cmd);
  printf("\n");
  printf(
"  "B(-a)" <"U(SUB-OPTION)">[: ...]\n");
  printf("\n");
  printf(
"                Define and setup the algorithm to use.\n"
"                Available SUB-OPTIONS are:\n");
  printf("\n");
  printf(
"                "B(algo=)"<"U(rwalk)"|"U(wos)">\n");
  printf("\n");
  printf(
"                    algorithm to use. "U(wos)" and "U(rwalk)" means for `walk on sphere'\n"
"                    and `random walk', respectively. Default algorithm is `%s'.\n",
    algorithm_to_string(SWOS_ARGS_DEFAULT.algo));
  printf("\n");
  printf(
"                "B(epsilon=)U(REAL)"\n");
  printf("\n");
  printf(
"                    this option is only available for the "U(wos)" algorithm. It\n"
"                    defines the distance to the surface from which the\n"
"                    diffusion position is considered to lie on the surface.\n"
"                    Its default value is computed from the volume `V' and\n"
"                    the surface `S' of the super shape as\n"
"                    `epsilon = 0.002 * V / S'.\n");
  printf("\n");
  printf(
"                "B(delta=)U(REAL)"\n");
  printf("\n");
  printf(
"                    defines the maximum length of a diffusion step. For the\n"
"                    "U(rwalk)" algorithm, its default value is automatically\n"
"                    computed from the volume `V' and the surface `S' of the\n"
"                    super shape as `delta = 0.1 * V / S'.\n");
  printf("\n");
  printf(
"                    For the "U(wos)" algorithm, its default value is the minimum\n"
"                    distance from the super shape boundary to the position of\n"
"                    each diffusion step. If delta is defined, this distance\n"
"                    is then limited to the value of delta. If delta is negative,\n"
"                    the value used is `-delta * 0.1 * V / S'.\n");
  printf("\n");
  printf(
"  "B(-D)"            write the mesh of the super shape to standard output and exit.\n"
"                The data are written with respect to the obj file format.\n");
  printf("\n");
  printf(
"  "B(-h)"            display this help and exit.\n");
  printf("\n");
  printf(
"  "B(-n)" "U(NREALISATIONS)"\n");
  printf("\n");
  printf(
"                number of realisations. Its default value is %lu.\n",
    SWOS_ARGS_DEFAULT.nrealisations);
  printf("\n");
  printf(
"  "B(-p)" <"U(SUB - OPTION)">[: ...]\n");
  printf(
"                define the probe position relative to the origin.\n"
"                Available SUB-OPTIONS are:\n");
  printf("\n");
  printf(
"                "B(theta=)U(REAL)"\n");
  printf("\n");
  printf(
"                    elevation angle, in the range [ -PI  PI ].\n"
"                    Default is `%f'.\n", SWOS_ARGS_DEFAULT.theta);
  printf("\n");
  printf(
"                "B(phi=)U(REAL)"\n");
  printf("\n");
  printf(
"                    azimuth angle, in the range [ 0  2*PI [.\n"
"                    Default value is `%f'.\n", SWOS_ARGS_DEFAULT.phi);
  printf("\n");
  printf(
"                "B(ratio=)U(REAL)"\n");
  printf("\n");
  printf(
"                    define the distance of the probe from the origin, less than\n"
"                    1. Default value is `%f'.\n", SWOS_ARGS_DEFAULT.ratio);
  printf("\n");
  printf(
"                    If positive, the distance is computed from the radius of the\n"
"                    super shape along the direction defined by theta and phi as\n"
"                    `radius(theta, phi) * ratio'.\n");
  printf("\n");
  printf(
"                    If negative, the distance is computed from the volume `V',\n"
"                    the surface `S' and the radius of the super shape along the\n"
"                    direction defined by theta and phi as\n"
"                    `radius(theta, phi) - 0.1 * ratio * V / S'.\n"
"                    Its in an error if the resulting distance is negative.\n");
  printf("\n");
  printf(
"  "B(-r)" "U(RADIUS)"     radius of the super shape. Its default value is %g.\n",
    SWOS_ARGS_DEFAULT.shape_radius);
  printf("\n");
  printf(
"  "B(-s)" "U(NSLICES)"    number of subdivisions of the super shape around the Z axis.\n"
"                At least 8. Its default value is %lu.\n",
    SWOS_ARGS_DEFAULT.nslices);
  printf("\n");
  printf(
"  "B(-S)" "U(SHAPE)"      chose which super shape to use. Available shapes are:\n"
"                "U(spheroid)": a spheroidal shape.\n"
"                "U(discoid)": a flat discoidal shape.\n"
"                Its default value is %s.\n",
    shape_type_to_string(SWOS_ARGS_DEFAULT.shape_type));
  printf("\n");
  printf(
"  "B(-t)" "U(NTHREADS)"   hint on the number of threads to use. By default use as many\n"
"                threads as CPU cores.\n");
  printf("\n");
  printf(
"Copyright (C) 2019, 2023 |Méso|Star>, This is free software released under the\n"
"GNU GPL license, version 3 or later. You are free to change or redistribute it\n"
"under certain conditions <http://gnu.org/licenses/gpl.html>\n");

  #undef B
  #undef U
}

static res_T
parse_algorithm_option(const char* str, struct swos_args* args)
{
  char buf[256] = {0};
  char* key = NULL;
  char* val = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(str && args);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/) {
    res = RES_MEM_ERR;
    goto error;
  }

  strcpy(buf, str);
  key = strtok_r(buf, "=", &tk_ctx);
  val = strtok_r(NULL, "", &tk_ctx);
  if(!key || !val) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(!strcmp(key, "algo")) {
    if(!strcmp(val, "rwalk")) {
      args->algo = SWOS_ALGO_RWALK;
    } else if(!strcmp(val, "wos")) {
      args->algo = SWOS_ALGO_WOS;
    } else {
      res = RES_BAD_ARG;
      goto error;
    }
  } else if(!strcmp(key, "delta")) {
    res = cstr_to_float(val, &args->delta);
    /* Note that delta can be negative for WoS algorithm to mean that it must
     * be computed from the super shape */
    if(res == RES_OK && args->delta == 0) res = RES_BAD_ARG;
    if(res != RES_OK) goto error;
  } else if(!strcmp(key, "epsilon")) {
    res = cstr_to_float(val, &args->epsilon);
    if(res == RES_OK && args->epsilon <= 0) res = RES_BAD_ARG;
    if(res != RES_OK) goto error;
  } else {
    res =  RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_algorithm(const char* str, struct swos_args* args)
{
  char buf[256] = {0};
  char* tk = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(str && args);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/) {
    res = RES_MEM_ERR;
    goto error;
  }

  strcpy(buf, str);
  tk = strtok_r(buf, ":", &tk_ctx);
  do {
    res = parse_algorithm_option(tk, args);
    if(res != RES_OK) goto error;
    tk = strtok_r(NULL, ":", &tk_ctx);
  } while(tk);

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_super_shape(const char* str, struct swos_args* args)
{
  ASSERT(str && args);
  if(!strcmp(str, "spheroid")) args->shape_type = SHAPE_SPHEROID;
  else if(!strcmp(str, "discoid")) args->shape_type = SHAPE_DISCOID;
  else return RES_BAD_ARG;
  return RES_OK;
}

static res_T
parse_probe_pos_option(const char* str, struct swos_args* args)
{
  char buf[256] = { 0 };
  char* key = NULL;
  char* val = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(str && args);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/) {
    res = RES_MEM_ERR;
    goto error;
  }

  strcpy(buf, str);
  key = strtok_r(buf, "=", &tk_ctx);
  val = strtok_r(NULL, "", &tk_ctx);
  if(!key || !val) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(!strcmp(key, "theta")) {
    res = cstr_to_float(val, &args->theta);
    if(res == RES_OK && fabs(args->theta) > 0.5 * PI) res = RES_BAD_ARG;
    if(res != RES_OK) goto error;
  } else if(!strcmp(key, "phi")) {
    res = cstr_to_float(val, &args->phi);
    if(res == RES_OK && (args->phi < 0 || args->phi >= 2 * PI))
      res = RES_BAD_ARG;
    if(res != RES_OK) goto error;
  } else if(!strcmp(key, "ratio")) {
    res = cstr_to_float(val, &args->ratio);
    /* Note that ratio can be negative to mean a distance from the super
     * shape boundary */
    if(res == RES_OK && args->ratio >= 1) res = RES_BAD_ARG;
    if(res != RES_OK) goto error;
  } else {
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_probe_position(const char* str, struct swos_args* args)
{
  char buf[256] = { 0 };
  char* tk = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(str && args);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/) {
    res = RES_MEM_ERR;
    goto error;
  }

  strcpy(buf, str);
  tk = strtok_r(buf, ":", &tk_ctx);
  do {
    res = parse_probe_pos_option(tk, args);
    if(res != RES_OK) goto error;
    tk = strtok_r(NULL, ":", &tk_ctx);
  } while (tk);

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Args functions
 ******************************************************************************/
const char*
algorithm_to_string(const enum swos_algorithm algo)
 {
   const char* str = NULL;
   switch (algo) {
   case SWOS_ALGO_WOS: str = "wos"; break;
   case SWOS_ALGO_RWALK: str = "rwalk"; break;
   default: FATAL("Unreachable code\n"); break;
   }
   return str;
 }

const char*
shape_type_to_string(const enum super_shape_type type)
 {
   const char* str = NULL;
   switch (type) {
   case SHAPE_SPHEROID: str = "spheroid"; break;
   case SHAPE_DISCOID: str = "discoid"; break;
   default: FATAL("Unreachable code\n"); break;
   }
   return str;
 }

res_T
swos_args_init(struct swos_args* args, int argc, char* argv[])
{
  int opt;
  res_T res = RES_OK;
  ASSERT(args && argc && argv);

  *args = SWOS_ARGS_DEFAULT;
  while((opt = getopt(argc, argv, "a:Dhn:p:r:s:S:t:")) != -1) {
    switch(opt) {
      case 'a':
        res = parse_algorithm(optarg, args);
        break;
      case 'D':
        args->dump_mesh = 1;
        break;
      case 'h':
        print_help(argv[0]);
        args->quit = 1;
        goto exit;
      case 'n':
        res = cstr_to_ulong(optarg, &args->nrealisations);
        if(res == RES_OK && args->nrealisations == 0) res = RES_BAD_ARG;
        break;
      case 'p':
        res = parse_probe_position(optarg, args);
        break;
      case 'r':
        res = cstr_to_float(optarg, &args->shape_radius);
        if(res == RES_OK && args->shape_radius <= 0) res = RES_BAD_ARG;
        break;
      case 's':
        res = cstr_to_ulong(optarg, &args->nslices);
        if(res == RES_OK && args->nslices < 8) res = RES_BAD_ARG;
        break;
      case 'S':
        res = parse_super_shape(optarg, args);
        break;
      case 't':
        res = cstr_to_uint(optarg, &args->nthreads);
        if(res == RES_OK && !args->nthreads) res = RES_BAD_ARG;
        break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

  if(args->delta == 0) { /* Default value */
    switch(args->algo) {
      case SWOS_ALGO_RWALK:
        /* Compute delta from the super shape */
        args->delta = -1;
        break;
      case SWOS_ALGO_WOS:
        /* Setup the search radius to infinity */
        args->delta = (float)INF;
        break;
      default: FATAL("Unreachable code\n"); break;
    }
  }

exit:
  return res;
error:
  swos_args_release(args);
  goto exit;
}

void
swos_args_release(struct swos_args* args)
{
  ASSERT(args);
  *args = SWOS_ARGS_DEFAULT;
}

