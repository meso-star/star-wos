/* Copyright (C) 2019, 2023 |Méso|Star> (contact@@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "swos.h"
#include "swos_args.h"

#include <rsys/mem_allocator.h>

int
main(int argc, char* argv[])
{
  struct swos swos = SWOS_NULL;
  struct swos_args args = SWOS_ARGS_DEFAULT;
  int err = 0;
  res_T res = RES_OK;

  /* Parse the command line arguments */
  res = swos_args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
  if(args.quit) goto exit;

  res = swos_init(&mem_default_allocator, &args, &swos);
  if(res != RES_OK) goto error;

  res = swos_run(&swos);
  if(res != RES_OK) goto error;

exit:
  swos_args_release(&args);
  swos_release(&swos);
  if(MEM_ALLOCATED_SIZE(&mem_default_allocator)) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long)MEM_ALLOCATED_SIZE(&mem_default_allocator));
    err = -1;
  }
  return err;
error:
  err = -1;
  goto exit;
}
