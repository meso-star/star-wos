/* Copyright (C) 2019, 2023 |Méso|Star> (contact@@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "swos.h"

#include <star/s3d.h>
#include <star/s3dut.h>
#include <star/ssp.h>

#include <rsys/clock_time.h>
#include <rsys/mem_allocator.h>

#include <omp.h>
#include <math.h>

struct estimation {
  double E; /* Expected value */
  double V; /* Variance */
  double SE; /* Standard error */
};
#define ESTIMATION_NULL__ { 0, 0, 0 }
static const struct estimation ESTIMATION_NULL = ESTIMATION_NULL__;

struct accum {
  double sum;
  double sum2;
  size_t count;
};
#define ACCUM_NULL__ { 0, 0, 0 }
static const struct accum ACCUM_NULL = ACCUM_NULL__;

static INLINE double
trilinear_value
  (const float pos[3],
   const float aabb_low[3],
   const float aabb_upp[3])
{
  const float a = 333;
  const float b = 432;
  const float c = 579;
  float x, y, z;
  ASSERT(pos && aabb_low && aabb_upp);

  x = (pos[0] - aabb_low[0]) / (aabb_upp[0] - aabb_low[0]);
  y = (pos[1] - aabb_low[1]) / (aabb_upp[1] - aabb_low[1]);
  z = (pos[2] - aabb_low[2]) / (aabb_upp[2] - aabb_low[2]);

  return a*x + b*y + c*z;
}

static INLINE int
aabb_holds_point(const float low[3], const float upp[3], const float pos[3])
{
  ASSERT(low && upp && pos);
  return pos[0] >= low[0] && pos[0] <= upp[0]
      && pos[1] >= low[1] && pos[1] <= upp[1]
      && pos[2] >= low[2] && pos[2] <= upp[2];
}

/*******************************************************************************
 * Random walk realisation
 ******************************************************************************/
static res_T
random_walk
  (const float probe_pos[3],
   const float delta,
   struct ssp_rng* rng,
   struct s3d_scene_view* view,
   double* value,
   size_t* out_nsteps)
{
  const float RAY_RANGE_MAX_SCALE = 1.001f;
  size_t nsteps = 0;
  float low[3], upp[3];
  float pos[3];
  float range[2];

  res_T res = RES_OK;
  ASSERT(probe_pos && delta > 0 && rng && view && value && out_nsteps);

  f3_set(pos, probe_pos);
  S3D(scene_view_get_aabb(view, low, upp));

  range[0] = 0;
  range[1] = delta * RAY_RANGE_MAX_SCALE;

  for(;;) {
    struct s3d_hit hit0, hit1;
    float u0[3], u1[3];
    float delta_solid;

    /* Sample a uniform direction */
    ssp_ran_sphere_uniform_float(rng, u0, NULL);
    f3_minus(u1, u0);

    /* Trace the sampled direction and its opposite */
    S3D(scene_view_trace_ray(view, pos, u0, range, NULL, &hit0));
    S3D(scene_view_trace_ray(view, pos, u1, range, NULL, &hit1));

    if(S3D_HIT_NONE(&hit0) && S3D_HIT_NONE(&hit1)) {
      delta_solid = delta;
    } else {
      delta_solid = MMIN(hit0.distance, hit1.distance);
    }

    if(!S3D_HIT_NONE(&hit0)
    && delta_solid != hit0.distance
    && eq_eps(hit0.distance, delta_solid, delta*0.1)) {
    /* Set delta to the main hit distance if it is roughly equal to it in order
     * to avoid numerical issues on moving along the main direction. */
      delta_solid = hit0.distance;
    }

    /* Move to the new position */
    pos[0] += u0[0]*delta_solid;
    pos[1] += u0[1]*delta_solid;
    pos[2] += u0[2]*delta_solid;

    /* Check invalid path */
    if(!aabb_holds_point(low, upp, pos)) {
      res = RES_BAD_OP;
      goto error;
    }

    ++nsteps;

    /* When delta is roughly equal to the hit distance, assume that the
     * conductive path reaches the boundary */
    if(eq_epsf(delta_solid, hit0.distance, delta_solid*1.e-4f)) {
      *value = trilinear_value(pos, low, upp);
      *out_nsteps = nsteps;
      break;
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Walk on Sphere realisation
 ******************************************************************************/
static res_T
walk_on_sphere
  (const float probe_pos[3],
   const float delta,
   const float epsilon,
   struct ssp_rng* rng,
   struct s3d_scene_view* view,
   double* weight,
   size_t* out_nsteps)
{
  size_t nsteps = 0;
  float low[3], upp[3];
  float pos[3], tmp[3];
  res_T res = RES_OK;
  ASSERT(probe_pos && delta > 0 && epsilon > 0 && rng && weight && out_nsteps);

  f3_set(pos, probe_pos);
  S3D(scene_view_get_aabb(view, low, upp));

  for(;;) {
    struct s3d_hit hit;
    float u[3];
    float dst;

    S3D(scene_view_closest_point(view, pos, delta, NULL, &hit));
    if(S3D_HIT_NONE(&hit)) {
      dst = delta;
    } else {
      dst = hit.distance;
    }

    if(dst < epsilon) { /* The path reaches the boundary */
      *weight = trilinear_value(pos, low, upp);
      *out_nsteps = nsteps;
      break;
    }

    /* Sample a uniform direction to move toward */
    ssp_ran_sphere_uniform_float(rng, u, NULL);

    /* Move to the new position */
    tmp[0] = pos[0] + u[0] * dst;
    tmp[1] = pos[1] + u[1] * dst;
    tmp[2] = pos[2] + u[2] * dst;

    /* If the float accuracy cannot do this step */
    if(f3_eq(tmp, pos)) {
      *weight = trilinear_value(pos, low, upp);
      *out_nsteps = nsteps;
      break;
    } else {
      pos[0] = tmp[0];
      pos[1] = tmp[1];
      pos[2] = tmp[2];
    }

    /* Check invalid path */
    if(!aabb_holds_point(low, upp, pos)) {
      res = RES_BAD_OP;
      goto error;
    }

    ++nsteps;
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE void
accum_add(struct accum* accum, double val)
{
  ASSERT(accum);
  accum->sum += val;
  accum->sum2 += val*val;
  ++accum->count;
}

static INLINE void
accum_get_estimation
  (const struct accum* accum,
   struct estimation* estimation)
{
  ASSERT(accum && estimation);
  estimation->E = accum->sum/(double)accum->count;
  estimation->V = accum->sum2/(double)accum->count - estimation->E*estimation->E;
  estimation->SE = sqrt(estimation->V/(double)accum->count);
}

static void
accums_redux(struct accum* dst, const struct accum accs[], const size_t naccs)
{
  struct accum tmp = ACCUM_NULL;
  size_t i;
  ASSERT(dst && accs && naccs);
  FOR_EACH(i, 0, naccs) {
    tmp.sum += accs[i].sum;
    tmp.sum2 += accs[i].sum2;
    tmp.count += accs[i].count;
  }
  *dst = tmp;
}

static void
dump_mesh(struct swos* swos, FILE* stream)
{
  struct s3dut_mesh_data mesh_data;
  size_t i;
  ASSERT(swos && stream);

  S3DUT(mesh_get_data(swos->mesh, &mesh_data));

  /* Write the list of vertices */
  FOR_EACH(i, 0, mesh_data.nvertices) {
    const int err = fprintf(stream,
      "v %f %f %f\n",
      mesh_data.positions[i*3+0],
      mesh_data.positions[i*3+1],
      mesh_data.positions[i*3+2]);
    ASSERT(err > 0); (void)err;
  }

  /* Write the list of faces */
  FOR_EACH(i, 0, mesh_data.nprimitives) {
    const int err = fprintf(stream,
      "f %lu %lu %lu\n",
      (unsigned long)mesh_data.indices[i*3+0] + 1,
      (unsigned long)mesh_data.indices[i*3+1] + 1,
      (unsigned long)mesh_data.indices[i*3+2] + 1);
    ASSERT(err > 0); (void)err;
  }
}

static res_T
setup_position(struct swos* swos, struct s3d_scene_view* view)
{
  struct s3d_hit hit;
  const float pos[3] = {0,0,0};
  float dir[3] = {0,0,0};
  float range[2] = {0, FLT_MAX};
  float dst;
  res_T res = RES_OK;
  ASSERT(swos && view);

  /* Setup probe position */
  if(swos->ratio == 0) {
    f3(swos->probe_pos, 0, 0, 0);
    goto exit;
  }

  /* cosf sinf are C99 and > only */
  dir[0] = (float)(cos((double)swos->theta)*cos((double)swos->phi));
  dir[1] = (float)(cos((double)swos->theta)*sin((double)swos->phi));
  dir[2] = (float)(sin((double)swos->theta));

  res = s3d_scene_view_trace_ray(view, pos, dir, range, NULL, &hit);
  if(res != RES_OK || S3D_HIT_NONE(&hit)) {
    fprintf(stderr, "Cannot setup probe position\n");
    goto error;
  }

  if(swos->ratio > 0) {
    dst = hit.distance * swos->ratio;
    f3_mulf(swos->probe_pos, dir, dst);
  } else {
    dst = hit.distance - (-swos->ratio) * swos->delta_rwalk;
    if(dst > 0) {
      f3_mulf(swos->probe_pos, dir, dst);
    } else {
      fprintf(stderr,
        "Probe position ratio %f defined a negative distance: %f\n",
        swos->ratio, dst);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
compute(struct swos* swos)
{
  char buf[128];
  struct time t0, t1;
  struct estimation estimation = ESTIMATION_NULL;
  struct s3dut_mesh_data mesh_data;
  struct accum* val_accs = NULL;
  struct accum* nsteps_accs = NULL;
  struct ssp_rng_proxy* proxy = NULL;
  struct ssp_rng** rngs = NULL;
  struct s3d_scene_view* view = NULL;
  float low[3], upp[3];
  int64_t irealisation;
  size_t nthreads;
  size_t i;
  double ref;
  int warn;
  res_T res = RES_OK;
  ASSERT(swos);

  nthreads = swos->nthreads;

  /* Allocate the per thread variables */
  rngs = MEM_CALLOC(swos->allocator, nthreads, sizeof(*rngs));
  if(!rngs) { res = RES_MEM_ERR; goto error; }
  val_accs = MEM_CALLOC(swos->allocator, nthreads, sizeof(*val_accs));
  if(!val_accs) { res = RES_MEM_ERR; goto error; }
  nsteps_accs = MEM_CALLOC(swos->allocator, nthreads, sizeof(*nsteps_accs));
  if(!nsteps_accs) { res = RES_MEM_ERR; goto error; }

  /* Create the per thread RNGs */
  SSP(rng_proxy_create(swos->allocator, SSP_RNG_MT19937_64, nthreads, &proxy));
  FOR_EACH(i, 0, swos->nthreads) {
    SSP(rng_proxy_create_rng(proxy, i, &rngs[i]));
  }

  /* Setup the per thread accumulators */
  FOR_EACH(i, 0, swos->nthreads) {
    val_accs[i] = ACCUM_NULL;
    nsteps_accs[i] = ACCUM_NULL;
  }

  /* Setup the scene view */
  S3D(scene_view_create(swos->scn, S3D_TRACE, &view));
  S3D(scene_view_get_aabb(view, low, upp));

  /* Compute the maximum geometry distance of any probe point */
  swos->max_shape_radius = (float)INF;
  FOR_EACH(i, 0, 3) {
    float dst = 0.5f * (upp[i] - low[i]);
    swos->max_shape_radius = MMIN(swos->max_shape_radius, dst);
  }

  /* Setup the probe position */
  res = setup_position(swos, view);
  if(res != RES_OK) goto error;

  /* Print some informations */
  S3DUT(mesh_get_data(swos->mesh, &mesh_data));
  printf("Running the '%s' algorithm\n", algorithm_to_string(swos->algo));
  printf("Shape %s, polycount: %lu\n",
    shape_type_to_string(swos->shape_type), (unsigned long)mesh_data.nprimitives);
  printf("Delta: %g\n", swos->delta);
  if(swos->algo == SWOS_ALGO_WOS) {
    printf("Epsilon: %g\n", swos->epsilon);
  }

  omp_set_num_threads((int)swos->nthreads);
  time_current(&t0);
  #pragma omp parallel for schedule(static)
  for(irealisation=0; irealisation<(int64_t)swos->nrealisations; ++irealisation) {
    const int ithread = omp_get_thread_num();
    struct accum* val_acc = &val_accs[ithread];
    struct accum* nsteps_acc = &nsteps_accs[ithread];
    struct ssp_rng* rng = rngs[ithread];
    double value = 0;
    size_t nsteps = 0;
    res_T res_local = RES_OK;

    switch(swos->algo) {
      case SWOS_ALGO_RWALK:
        res_local = random_walk
          (swos->probe_pos, swos->delta, rng, view, &value, &nsteps);
        break;
      case SWOS_ALGO_WOS:
        res_local = walk_on_sphere
          (swos->probe_pos, swos->delta, swos->epsilon, rng, view, &value, &nsteps);
        break;
      default: FATAL("Unreachable code.\n"); break;
    }
    if(res_local != RES_OK) continue; /* The realisations failed */

    accum_add(val_acc, value);
    accum_add(nsteps_acc, (double)nsteps);
  }
  time_current(&t1);
  time_sub(&t0, &t1, &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));

  accums_redux(&val_accs[0], val_accs, nthreads);
  accums_redux(&nsteps_accs[0], nsteps_accs, nthreads);

  /* Print result */
  accum_get_estimation(&val_accs[0], &estimation);
  printf("Probe position: %g %g %g\n", SPLIT3(swos->probe_pos));
  printf("Super shape extend: %g\n", swos->max_shape_radius);
  /* Print estimation in reverse color if more than 3 sigmas */
  ref = trilinear_value(swos->probe_pos, low, upp);
  warn = fabs(ref - estimation.E) > 3 * estimation.SE;
  printf(warn ? "\x1b[7mValue = %g ~ %g +/- %g\x1b[0m " : "Value = %g ~ %g +/- %g ",
    ref, /* Reference value */
    estimation.E, /* Expected value */
    estimation.SE); /* Standard error */
  printf(warn ? "\x1b[7m(#failures: %lu/%lu)\x1b[0m\n" : "(#failures: %lu/%lu)\n",
    (unsigned long)(swos->nrealisations - val_accs[0].count), /* #failures */
    (unsigned long)swos->nrealisations);
  accum_get_estimation(&nsteps_accs[0], &estimation);
  printf("Number of diffusive steps: %g +/- %g\n", estimation.E, estimation.SE);
  /* Print failures in reverse color if not 0 */
  warn = swos->nrealisations != val_accs[0].count;
  printf("Elapsed time: %s\n", buf);

exit:
  if(view) S3D(scene_view_ref_put(view));
  if(proxy) SSP(rng_proxy_ref_put(proxy));
  if(val_accs) MEM_RM(swos->allocator, val_accs);
  if(nsteps_accs) MEM_RM(swos->allocator, nsteps_accs);
  if(rngs) {
    FOR_EACH(i, 0, nthreads) SSP(rng_ref_put(rngs[i]));
    MEM_RM(swos->allocator, rngs);
  }
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Star-WoS functions
 ******************************************************************************/
res_T
swos_run(struct swos* swos)
{
  res_T res = RES_OK;
  ASSERT(swos);
  if(swos->dump_mesh) {
    dump_mesh(swos, stdout); /* Write the geometry to standard output and exit */
  } else {
    res = compute(swos); /* Launch the Monte Carlo simulation */
    if(res != RES_OK) goto error;
  }
exit:
  return res;
error:
  goto exit;
}

