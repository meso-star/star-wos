/* Copyright (C) 2019, 2023 |Méso|Star> (contact@@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "swos.h"

#include <star/s3d.h>
#include <star/s3dut.h>

#include <rsys/math.h>
#include <rsys/mem_allocator.h>
#include <omp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
mesh_get_position(const unsigned ivert, float pos[3], void* ctx)
{
  struct s3dut_mesh_data* msh = ctx;
  ASSERT(pos && msh && ivert < msh->nvertices);
  pos[0] = (float)msh->positions[ivert*3+0];
  pos[1] = (float)msh->positions[ivert*3+1];
  pos[2] = (float)msh->positions[ivert*3+2];
}

static void
mesh_get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
  struct s3dut_mesh_data* msh = ctx;
  ASSERT(ids && msh && itri < msh->nprimitives);
  /* Flip the indices to ensure that the normal points into the super shape */
  ids[0] = (unsigned)msh->indices[itri*3+0];
  ids[1] = (unsigned)msh->indices[itri*3+2];
  ids[2] = (unsigned)msh->indices[itri*3+1];
}

/*******************************************************************************
 * Star-WoS functions
 ******************************************************************************/
res_T
swos_init
  (struct mem_allocator* allocator,
   struct swos_args* args,
   struct swos* swos)
{
  struct s3dut_super_formula f0 = S3DUT_SUPER_FORMULA_NULL;
  struct s3dut_super_formula f1 = S3DUT_SUPER_FORMULA_NULL;
  struct s3dut_mesh_data mesh_data;
  struct s3d_vertex_data vdata = S3D_VERTEX_DATA_NULL;
  struct s3d_shape* shape = NULL;
  struct s3d_scene_view* view;
  float V, S;

  ASSERT(swos && args);
  *swos = SWOS_NULL;

  swos->allocator = allocator ? allocator : &mem_default_allocator;

  /* Use Star-3DUT to generate the mesh of the super shape */
  switch (args->shape_type) {
  case SHAPE_SPHEROID:
    f0.A = 1.5; f0.B = 1; f0.M = 11; f0.N0 = 1; f0.N1 = 1; f0.N2 = 2;
    f1.A = 1; f1.B = 2; f1.M = 3.6; f1.N0 = 1; f1.N1 = 2; f1.N2 = 0.7;
    break;
  case SHAPE_DISCOID:
    f0.A = 100; f0.B = 1; f0.M = 0; f0.N0 = 1; f0.N1 = 1; f0.N2 = 1;
    f1.A = 100; f1.B = 1; f1.M = 0; f1.N0 = 1; f1.N1 = 1; f1.N2 = 1;
    break;
  default: FATAL("Unreachable code\n"); break;
  }
  S3DUT(create_super_shape(swos->allocator, &f0, &f1, args->shape_radius,
    (unsigned)args->nslices, (unsigned)args->nslices/2, &swos->mesh));
  S3DUT(mesh_get_data(swos->mesh, &mesh_data));

  /* Setup the Star-3D scene */
  S3D(device_create(NULL, swos->allocator, 0/*verbose*/, &swos->s3d));
  S3D(scene_create(swos->s3d, &swos->scn));
  S3D(shape_create_mesh(swos->s3d, &shape));
  S3D(scene_attach_shape(swos->scn, shape));

  /* Setup the Star-3D shape */
  vdata.usage = S3D_POSITION;
  vdata.type = S3D_FLOAT3;
  vdata.get = mesh_get_position;
  S3D(mesh_setup_indexed_vertices(shape, (unsigned)mesh_data.nprimitives,
    mesh_get_indices, (unsigned)mesh_data.nvertices, &vdata, 1, &mesh_data));
  S3D(shape_ref_put(shape));

  /* Define swos parameters */
  swos->nrealisations = args->nrealisations;
  swos->algo = args->algo;
  swos->shape_type = args->shape_type;
  swos->dump_mesh = args->dump_mesh;
  swos->delta = args->delta;
  swos->epsilon = args->epsilon;
  swos->theta = args->theta;
  swos->phi = args->phi;
  swos->ratio = args->ratio;
  swos->nthreads = MMIN(args->nthreads, (unsigned)omp_get_num_procs());

  /* Compute delta_rwalk */
  S3D(scene_view_create(swos->scn, 0, &view));
  S3D(scene_view_compute_volume(view, &V));
  S3D(scene_view_compute_area(view, &S));
  swos->delta_rwalk = 0.1f * V / S;
  S3D(scene_view_ref_put(view));
  /* If delta is negative its a number of delta_rwalk */
  if(swos->delta < 0) swos->delta *= -swos->delta_rwalk;
  /* If epsilon is negative (i.e. undefined) set default value */
  if(swos->epsilon < 0) swos->epsilon = 0.02f * swos->delta_rwalk;

  return RES_OK;
}

void
swos_release(struct swos* swos)
{
  ASSERT(swos);
  if(swos->mesh) S3DUT(mesh_ref_put(swos->mesh));
  if(swos->s3d) S3D(device_ref_put(swos->s3d));
  if(swos->scn) S3D(scene_ref_put(swos->scn));
}

