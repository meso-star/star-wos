# Star-WalkOnSphere

This program is used to test the `closest_point` operator provided by the
Star-3D library. To do so, it implements a walk on sphere algorithm into a
Super Shape and compare it to a regular discretised diffusive random walk.

## How to build

Star-WoS relies on [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) package to build. It also
depends on the
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-3D](https://gitlab.com/meso-star/star-3d/),
[Star-3DUT](https://gitlab.com/meso-star/star-3dut/),
[Star-SP](https://gitlab.com/meso-star/star-sp/) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as all the aforementioned prerequisites. Then generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directory of its dependencies.

## License

Copyright (C) 2019, 2023 [|Meso|Star>](http://www.meso-star.com). Star-WoS is
free software released under the GPL v3+ license: GNU GPL version 3 or later.
You are welcome to redistribute it under certain conditions; refer to the
COPYING file for details.

